import express from 'express';
import expressAsyncHandler from 'express-async-handler';
import Order from '../models/order.js';
import { isAuth, isAdmin } from '../auth.js';
import User from '../models/user.js';
import Product from '../models/product.js';

const orderRoutes = express.Router();
orderRoutes.post(
  '/',
  isAuth,
  expressAsyncHandler(async (request, response) => {
    const newOrder = new Order({
      orderItems: request.body.orderItems.map((item) => ({
        ...item,
        product: item._id,
      })),
      shippingAddress: request.body.shippingAddress,
      paymentMethod: request.body.paymentMethod,
      itemsPrice: request.body.itemsPrice,
      shippingPrice: request.body.shippingPrice,
      taxPrice: request.body.taxPrice,
      totalPrice: request.body.totalPrice,
      user: request.user._id,
    });

    const order = await newOrder.save();
    response.status(201).send({ message: 'New Order Created', order });
  })
);

orderRoutes.get(
  '/summary',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    const orders = await Order.aggregate([
      {
        $group: {
          _id: null,
          numOrders: { $sum: 1 },
          totalSales: { $sum: '$totalPrice' },
        },
      },
    ]);
    const users = await User.aggregate([
      {
        $group: {
          _id: null,
          numUsers: { $sum: 1 },
        },
      },
    ]);
    const dailyOrders = await Order.aggregate([
      {
        $group: {
          _id: { $dateToString: { format: '%Y-%m-%d', date: '$createdAt' } },
          orders: { $sum: 1 },
          sales: { $sum: '$totalPrice' },
        },
      },
      { $sort: { _id: 1 } },
    ]);
    const productCategories = await Product.aggregate([
      {
        $group: {
          _id: '$category',
          count: { $sum: 1 },
        },
      },
    ]);
    res.send({ users, orders, dailyOrders, productCategories });
  })
);

orderRoutes.get(
  '/mine',
  isAuth,
  expressAsyncHandler(async (req, res) => {
    const orders = await Order.find({ user: req.user._id });
    res.send(orders);
  })
);

orderRoutes.get(
  '/:id',
  isAuth,
  expressAsyncHandler(async (request, response) => {
    const order = await Order.findById(request.params.id);
    if (order) {
      response.send(order);
    } else {
      response.status(404).send({ message: 'No Order Found!' });
    }
  })
);

export default orderRoutes;
