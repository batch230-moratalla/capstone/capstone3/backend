import express from 'express';
import Product from '../models/product.js';
import { isAuth, isAdmin } from '../auth.js';
import expressAsyncHandler from 'express-async-handler';

const productRoutes = express.Router();

productRoutes.get('/', async (request, response) => {
  const products = await Product.find();
  response.send(products);
});

productRoutes.post(
  '/',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    const newProduct = new Product({
      name: 'sample name ' + Date.now(),
      slug: 'sample-name-' + Date.now(),
      image: '/images/p1.jpg',
      price: 0,
      category: 'sample category',
      brand: 'sample brand',
      stock: 0,
      reviews: 0,
      rating: 0,
      description: 'sample description',
    });
    const product = await newProduct.save();
    res.send({ message: 'Product Created', product });
  })
);
productRoutes.delete(
  '/:id',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id);
    if (product) {
      await product.deleteOne();
      res.send({ message: 'Product Deleted' });
    } else {
      res.status(404).send({ message: 'Product Not Found' });
    }
  })
);

productRoutes.put(
  '/:id',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    const productId = req.params.id;
    const product = await Product.findById(productId);
    if (product) {
      product.name = req.body.name;
      product.slug = req.body.slug;
      product.price = req.body.price;
      product.image = req.body.image;
      product.category = req.body.category;
      product.brand = req.body.brand;
      product.stock = req.body.stock;
      product.description = req.body.description;
      await product.save();
      res.send({ message: 'Product Updated' });
    } else {
      res.status(404).send({ message: 'Product Not Found' });
    }
  })
);

const PAGE_SIZE = 10;

productRoutes.get(
  '/admin',
  isAuth,
  isAdmin,
  expressAsyncHandler(async (req, res) => {
    const { query } = req;
    const page = query.page || 1;
    const pageSize = query.pageSize || PAGE_SIZE;

    const products = await Product.find()
      .skip(pageSize * (page - 1))
      .limit(pageSize);
    const countProducts = await Product.countDocuments();
    res.send({
      products,
      countProducts,
      page,
      pages: Math.ceil(countProducts / pageSize),
    });
  })
);

productRoutes.get('/slug/:slug', async (request, response) => {
  const product = await Product.findOne({ slug: request.params.slug });
  if (product) {
    response.send(product);
  } else {
    response.status(404).send({ message: 'Product not Found.' });
  }
});

productRoutes.get('/:id', async (request, response) => {
  const product = await Product.findById(request.params.id);
  if (product) {
    response.send(product);
  } else {
    response.status(404).send({ message: 'Product not Found.' });
  }
});

export default productRoutes;
