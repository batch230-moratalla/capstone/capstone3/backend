/* 
npm init -y - initialize
npm install express
npm install mongoose
npm install cors
npm install bcrypt
npm install jsonwebtoken
npm install express-async-handler - error handler
touch .gitignore
*/

import express, { request } from 'express';
import mongoose from 'mongoose';
import productRoutes from './routes/productRoutes.js';
import cors from 'cors';
import userRoutes from './routes/userRoutes.js';
import orderRoutes from './routes/orderRoutes.js';

const app = express();
const port = process.env.PORT || 5000;

mongoose.connect(
  'mongodb+srv://admin:admin@batch230.1dtuoqz.mongodb.net/foodtrip?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () =>
  console.log('You are now connected to the cloud database API.')
);

// app.use(express.static(__dirname));

// app.get('/*', function (req, res) {
//   res.sendFile(path.join(__dirname, 'index.html'));
// });

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/products', productRoutes);
app.use('/api/users', userRoutes);
app.use('/api/orders', orderRoutes);

app.use((error, request, response, next) => {
  response.status(500).send({ message: error.message });
});
app.listen(port, () => {
  console.log(`API is now online on port ${port}`);
});
/* 

localhost:5000/api/seed

*/
