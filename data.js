import bcrypt from 'bcryptjs';

const data = {
  users: [
    {
      name: 'admin',
      email: 'admin@gmail.com',
      password: bcrypt.hashSync('123456789'),
      isAdmin: true,
    },
    {
      name: 'guest',
      email: 'guest@gmail.com',
      password: bcrypt.hashSync('123456789'),
      isAdmin: false,
    },
  ],
  products: [
    {
      name: 'Porksilog',
      slug: 'Porksilog',
      category: 'Breakfast',
      image: '/images/b1.png',
      price: 120,
      stock: 25,
      description: 'Stir fried rice, with Beef Meat and fried Egg',
      brand: 'Fried',
      rating: 4.7,
      reviews: 10,
    },
    {
      name: 'Chicksilog',
      slug: 'Chicksilog',
      category: 'Breakfast',
      image: '/images/b2.png',
      price: 80,
      stock: 40,
      description: 'Stir fried rice, with fried Hotdog and fried Egg',
      brand: 'Fried',
      rating: 4.7,
      reviews: 10,
    },
    {
      // _id: '3',
      name: 'Hotsilog',
      slug: 'Hotsilog',
      category: 'Breakfast',
      image: '/images/b3.png',
      price: 120,
      stock: 0,
      description: 'Stir fried rice, with Beef Meat and fried Egg',
      brand: 'Fried',
      rating: 4.7,
      reviews: 10,
    },
    {
      // _id: '4',
      name: 'Bangsilog',
      slug: 'Bangsilog',
      category: 'Breakfast',
      image: '/images/b4.png',
      price: 150,
      stock: 10,
      description: 'Stir fried rice, with fried milk fish and fried egg',
      brand: 'Soup',
      rating: 4.9,
      reviews: 10,
    },
  ],
};

export default data;
